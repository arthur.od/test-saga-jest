import React from 'react';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';

import store from './store';
import App from './App';

test('renders "Load users" button', () => {
  render(
    <Provider store={store}>
      <App />
    </Provider>
  );
  expect(screen.getByRole('button')).toHaveTextContent(/Load users/i);
});
