/* eslint-disable no-undef */

export const host = process.env.REACT_APP_API_HOST?.replace(/\/+$/, '');
export const uri = (endpoint: string) =>
  `${host}/${endpoint.replace(/^\/+/, '')}`;

export const get = async function <T>(url: string): Promise<T> {
  const response = await fetch(uri(url), {
    method: 'GET',
  });
  if (!response.ok) {
    throw new Error('Failed to fetch data');
  }
  return await response.json();
};

export const post = async function <T>(
  url: string,
  payload: Record<string, any>
): Promise<T> {
  const response = await fetch(uri(url), {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload),
  });
  if (!response.ok) {
    throw new Error('Failed to send data');
  }
  return await response.json();
};

export const erase = async function (url: string): Promise<void> {
  const response = await fetch(uri(url), {
    method: 'DELETE',
  });
  if (!response.ok) {
    throw new Error('Failed to delete item');
  }
};
