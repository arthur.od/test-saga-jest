export interface IUser {
  id: number;
  name: string;
  avatar: string;
  createdAt: string;
}

export type TUsers = IUser[];

export interface IUsersState {
  loading: boolean;
  users: TUsers;
  lastError: string | null;
}
