import { erase, get } from 'services/api';

import { TUsers } from './types';

export let fetchUsers = async (): Promise<TUsers> =>
  (
    await get<
      { id: string; name: string; avatar: string; createdAt: string }[]
    >('users')
  ).map(({ id, ...rest }) => ({ id: +id, ...rest }));

export const deleteUser = (id: number) => erase(`users/${id}`);
