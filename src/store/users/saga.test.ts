/* eslint-disable no-import-assign */
import { runSaga } from 'redux-saga';

import { fetchUsers } from './saga';
import * as api from './api';
import { actions } from './reducer';
import { TUsers } from './types';

describe('fetch users', () => {
  afterAll(() => jest.clearAllMocks());

  test('fetch users without errors', async () => {
    const mockUsers: TUsers = [
      { id: 1, name: 'name', avatar: 'avatar1', createdAt: '2000-01-01' },
      { id: 2, name: 'name', avatar: 'avatar2', createdAt: '2001-01-01' },
    ];

    // @ts-ignore
    api.fetchUsers = jest.fn().mockResolvedValue(mockUsers);

    const dispatched: any = [];
    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => ({}),
      },
      fetchUsers as any
    ).toPromise();

    expect(api.fetchUsers).toHaveBeenCalledWith();

    expect(dispatched).toContainEqual(actions.load(true));
    expect(dispatched).toContainEqual(actions.update(mockUsers));
    expect(dispatched).toContainEqual(actions.load(false));
  });

  test('fetch users with errors', async () => {
    const errorMessage = 'Request failed';

    // @ts-ignore
    api.fetchUsers = jest.fn().mockRejectedValue({
      message: errorMessage,
    });

    const dispatched: any = [];
    await runSaga(
      {
        dispatch: action => dispatched.push(action),
        getState: () => ({}),
      },
      fetchUsers as any
    ).toPromise();

    expect(api.fetchUsers).toHaveBeenCalledWith();

    expect(dispatched).toContainEqual(actions.load(true));
    expect(dispatched).toContainEqual(actions.error(errorMessage));
    expect(dispatched).toContainEqual(actions.load(false));
  });

  // const gen = fetchUsers();
  // expect(gen.next().value).toEqual(put(actions.load(true)));
  // expect(gen.next().value).toEqual(call(api.fetchUsers));
  // expect(gen.next([]).value).toEqual(put(actions.update([])));
  // expect(gen.next().value).toEqual(put(actions.load(false)));
});
