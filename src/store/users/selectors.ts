import { createSelector } from '@reduxjs/toolkit';

import { AppStore } from '../reducers';

export const getUsersLoading = (state: AppStore) => state.users.loading;
export const getUsersList = (state: AppStore) => state.users.users;
export const getUsersError = (state: AppStore) => state.users.lastError;

export const getUserById = createSelector(
  [getUsersList, (_, id: number) => id],
  (users, userId) => users.find(({ id }) => id === userId)
);

export const getUsersCount = createSelector(
  getUsersList,
  users => users.length
);
