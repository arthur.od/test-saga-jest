const USERS = {
  LOAD: 'USERS_LOAD',
  DELETE: 'USER_DELETE',
  ADD: 'USER_ADD',
};

export default USERS;
