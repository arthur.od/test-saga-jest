import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { IUser, IUsersState, TUsers } from './types';

const initialState: IUsersState = {
  loading: false,
  users: [],
  lastError: null,
};

const { actions, reducer } = createSlice({
  name: 'users',
  initialState,
  reducers: {
    load: (state, action: PayloadAction<boolean>) => {
      state.loading = action.payload;
      if (action.payload) {
        state.lastError = null;
      }
    },

    update: (state, action: PayloadAction<TUsers>) => {
      state.users = action.payload;
    },

    add: (state, action: PayloadAction<IUser>) => {
      state.users.push(action.payload);
    },

    put: (state, action: PayloadAction<IUser>) => {
      const index = state.users.findIndex(({ id }) => id === action.payload.id);
      index > -1 && (state.users[index] = action.payload);
    },

    erase: (state, action: PayloadAction<number>) => {
      state.users = state.users.filter(({ id }) => id !== action.payload);
    },

    error: (state, action: PayloadAction<IUsersState['lastError']>) => {
      state.lastError = action.payload;
    },
  },
});

export { actions };
export default reducer;
