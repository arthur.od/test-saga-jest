import USERS from './constants';

export const fetchUsers = () => ({
  type: USERS.LOAD,
});

export const deleteUser = (id: number) => ({
  type: USERS.DELETE,
  payload: id,
});
