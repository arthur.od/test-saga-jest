import { call, put, takeLatest, takeEvery } from 'redux-saga/effects';

import * as api from './api';
import { actions } from './reducer';
import { TUsers } from './types';
import USERS from './constants';
import { PayloadAction } from '@reduxjs/toolkit';

export function* fetchUsers() {
  try {
    yield put(actions.load(true));
    const users: TUsers = yield call(api.fetchUsers);
    yield put(actions.update(users));
  } catch (e) {
    console.dir(e);
    yield put(actions.error((e as Error).message));
  } finally {
    yield put(actions.load(false));
  }
}

export function* deleteUser({ payload }: PayloadAction<number>) {
  try {
    yield put(actions.load(true));
    yield call(api.deleteUser, payload);
    yield put(actions.erase(payload));
  } catch (e) {
    yield put(actions.error((e as Error).message));
  } finally {
    yield put(actions.load(false));
  }
}

function* users() {
  yield takeLatest(USERS.LOAD, fetchUsers);
  yield takeEvery(USERS.DELETE, deleteUser);
}

export default users;
