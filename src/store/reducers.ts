import { combineReducers } from 'redux';

import users from './users/reducer';

const reducers = combineReducers({
  users,
});

export type AppStore = ReturnType<typeof reducers>;

export default reducers;
