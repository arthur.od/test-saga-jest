import React, { MouseEventHandler, useCallback, useEffect } from 'react';
import { connect, ConnectedProps } from 'react-redux';

import { fetchUsers, deleteUser } from 'store/users/actions';
import {
  getUsersCount,
  getUsersError,
  getUsersList,
  getUsersLoading,
} from 'store/users/selectors';

import styles from './App.module.scss';

const mapStateToProps = (state: any) => ({
  isLoading: getUsersLoading(state),
  users: getUsersList(state),
  usersCount: getUsersCount(state),
  error: getUsersError(state),
});

const mapActionsToProps = {
  fetchUsers,
  deleteUser,
};

const connector = connect(mapStateToProps, mapActionsToProps);

const App = connector(
  ({
    isLoading,
    users,
    usersCount,
    fetchUsers,
    deleteUser,
    error,
  }: ConnectedProps<typeof connector>) => {
    useEffect(() => {
      if (!error) return;
      alert(error);
    }, [error]);

    const deleteHandler = useCallback<MouseEventHandler>(
      ({ currentTarget }) => {
        const id = +((currentTarget as HTMLButtonElement).dataset.id || '');
        deleteUser(id);
      },
      []
    );

    if (isLoading) {
      return <div className={styles.loading}>Loading...</div>;
    }

    if (!usersCount) {
      return (
        <button type="button" className={styles.button} onClick={fetchUsers}>
          Load users
        </button>
      );
    }

    return (
      <>
        <div className={styles.count}>Users count: {usersCount}</div>
        <ul className={styles.users}>
          {users.map(({ id, avatar, name }) => (
            <li key={`user-${id}`}>
              <button
                type="button"
                data-id={id}
                title="Delete user"
                onClick={deleteHandler}
              >
                &#10060;
              </button>
              <img src={avatar} alt={name} />
              <span>{name}</span>
            </li>
          ))}
        </ul>
      </>
    );
  }
);

export default App;
